package com.example.controller;


import com.example.dto.DepartmentDto;
import com.example.service.DepartmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/departments")
public class DepartmentRestcontroller {

private DepartmentService service;

    public DepartmentRestcontroller(DepartmentService service) {
        this.service = service;
    }


    @PostMapping
    public ResponseEntity<DepartmentDto> createDepartment(@RequestBody DepartmentDto dto) throws Exception {
        return new ResponseEntity<>(service.create(dto), HttpStatus.CREATED);
    }


//    @GetMapping("/{id}")
//    public ResponseEntity<DepartmentDto> getById(@PathVariable("id") long id) throws  Exception
//    {
//        return  ResponseEntity.ok(service.getDepartmentById(id));
//    }


    @GetMapping("/list")
    public ResponseEntity<List<DepartmentDto>> getAllDepartment() throws  Exception{
        return ResponseEntity.ok(service.getList());
    }

    @PutMapping("{id}")
    public ResponseEntity<DepartmentDto> updateById(@RequestBody DepartmentDto dto,@PathVariable long id) throws Exception {
        return  ResponseEntity.ok(service.updateDepartment(dto,id));

    }


    @DeleteMapping("{id}")
    public ResponseEntity<String> deletebyId(@PathVariable("id") long id) throws  Exception
    {
        return  ResponseEntity.ok(service.delete(id));
    }


    @GetMapping("{code}")
    public ResponseEntity<DepartmentDto> getByCode(@PathVariable("code") String code) throws Exception{
        return ResponseEntity.ok(service.getByDepartmentCode(code));
    }
}
