package com.example.service;

import com.example.dto.DepartmentDto;

import java.util.List;

public interface DepartmentService {

    public DepartmentDto create(DepartmentDto dto) throws  Exception;

    public DepartmentDto getDepartmentById(long id ) throws  Exception;

    public List<DepartmentDto> getList() throws  Exception;

    public DepartmentDto updateDepartment(DepartmentDto dto,Long id) throws Exception;
    public String delete(long id) throws Exception;

    public DepartmentDto getByDepartmentCode(String code);
}
