package com.example.service;

import com.example.dto.DepartmentDto;
import com.example.entity.Department;
import com.example.repository.DepartmentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class DepartmentServiceImpl implements DepartmentService {


    private DepartmentRepository repo;
    private ModelMapper mapper;

    @Autowired
    public DepartmentServiceImpl(DepartmentRepository repo, ModelMapper mapper) {
        this.repo = repo;
        this.mapper = mapper;
    }

    @Override
    public DepartmentDto create(DepartmentDto dto) throws Exception {
        Department department = mapper.map(dto,Department.class);
        department = repo.save(department);
        return mapper.map(department,DepartmentDto.class);
    }

    @Override
    public DepartmentDto getDepartmentById(long id) throws Exception {
        Department department = repo.findById(id).orElseThrow(()->new RuntimeException("Department is not found with "+id));
        return mapper.map(department,DepartmentDto.class);
    }

    @Override
    public List<DepartmentDto> getList() throws Exception {
        List<Department> list = repo.findAll();
        List<DepartmentDto> dtos = list.stream()
                                        .map(x -> mapper.map(x, DepartmentDto.class))
                                        .collect(Collectors.toList());
        return dtos;
    }

    @Override
    public DepartmentDto updateDepartment(DepartmentDto dto, Long id) throws Exception {
        Department department = repo.findById(id).orElseThrow(()->new RuntimeException("Department is not found with "+id));
        department.setDepartmentName(dto.getDepartmentName());
        department.setDepartmentDesc(dto.getDepartmentDesc());
        department.setDepartmentCode(dto.getDepartmentCode());
        department = repo.save(department);
        return mapper.map(department,DepartmentDto.class);
    }

    @Override
    public String delete(long id) throws Exception {
        Department department = repo.findById(id).orElseThrow(()->new RuntimeException("Department is not found with "+id));
        repo.delete(department);
        return "Record has been deleted successfully with id : "+id;
    }

    @Override
    public DepartmentDto getByDepartmentCode(String code) {
        Department department = repo.findByDepartmentCode(code);
        return mapper.map(department,DepartmentDto.class);
    }
}
